package xmodule.core.utils


import org.fusesource.jansi.Ansi

fun header(text: String) {
    println("")
    println(Ansi.ansi().render("""@|white,bold *******|@ @|cyan,bold $text |@ @|white,bold ******* |@""").reset())
}

/**
 * Imprime uma saída no console com suporte a colorização via jansi.
 * @see https://github.com/fusesource/jansi
 */
fun echo(text: String) = println(Ansi.ansi().render(text).reset())
