package xmodule.core.command

import picocli.CommandLine

/**
 *
 */
abstract class AbstractInitializer: Runnable {

    @CommandLine.Unmatched
    protected lateinit var unmatched: List<String>
}
