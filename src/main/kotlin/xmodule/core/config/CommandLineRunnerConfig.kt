package xmodule.core.config

import org.fusesource.jansi.Ansi
import org.fusesource.jansi.AnsiConsole
import org.jline.console.impl.SystemRegistryImpl
import org.jline.reader.*
import org.jline.reader.impl.DefaultParser
import org.jline.terminal.TerminalBuilder
import org.jline.widget.AutosuggestionWidgets
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ResourceLoader
import picocli.CommandLine
import picocli.shell.jline3.PicocliCommands
import xmodule.core.command.AbstractCommand
import xmodule.core.command.AbstractInitializer
import xmodule.core.command.RootCommand
import xmodule.core.context.WorkspaceContext
import java.nio.file.Paths
import java.util.function.Supplier
import kotlin.system.exitProcess

@Configuration
class CommandLineRunnerConfig(
    private val workspaceContext: WorkspaceContext,
    private val factory: CommandLine.IFactory,
    private val rootCommand: RootCommand,
    private val commands: Set<AbstractCommand>,
    private val resourceLoader: ResourceLoader

    ) {
    @Autowired(required = false)
    private lateinit var initializer: AbstractInitializer

    private val log = LoggerFactory.getLogger(CommandLineRunnerConfig::class.java)

    /**
     * CommandLineRunner
     */
    @Bean
    @Profile("init")
    fun commandLineRunnerA(): CommandLineRunner {
        return CommandLineRunner { args ->
            if (!workspaceContext.isEmpty) {
                log.error("Não foi possível executar a inicialização da aplicação pois o diretório atual NÃO está vazio.")
                exitProcess(1)
            }
            CommandLine(initializer, factory).execute(*args)
            exitProcess(0)
        }
    }

    @Bean
    fun commandLineRunnerB(): CommandLineRunner {
        return CommandLineRunner { args ->
            if (!workspaceContext.hasDescritor) {
                log.error("Não foi possível executar o módulo pois o arquivo descritor xtool.yaml NÃO está presente no diretório atual.")
                exitProcess(1)
            }
//            val bannerResource = resourceLoader.getResource("classpath:xbanner.txt")
//            val bannerText = bannerResource.inputStream.bufferedReader().use { it.readText() }
//           Ansi.ansi().render(bannerText).reset()
            AnsiConsole.systemInstall();
            try {
//            println("""Módulo: ${Ansi.ansi().fgBrightCyan().bold().a(env.getProperty("module.name")).a("@").a(env.getProperty("module.version")).reset()}""")
//            println("")
                val workDir = Paths.get(System.getProperty("user.dir"))
                val pathSupplier = Supplier { workDir }
                // let picocli parse command line args and run the business logic
                val cmd = CommandLine(rootCommand, factory)
                commands.forEach { cmd.addSubcommand(it) }
                val picocliCommands = PicocliCommands(cmd)
                val parser = DefaultParser()
                val terminal = TerminalBuilder.builder().build()
                val systemRegistry = SystemRegistryImpl(parser, terminal, pathSupplier, null)
                systemRegistry.setCommandRegistries(picocliCommands)
                systemRegistry.register("help", picocliCommands)
                val reader = LineReaderBuilder.builder()
                    .terminal(terminal)
                    .completer(systemRegistry.completer())
                    .parser(parser)
                    .variable(LineReader.LIST_MAX, 50) // max tab completion candidates
                    .build()
                val widgets = AutosuggestionWidgets(reader)
                widgets.enable()
                val prompt = Ansi.ansi().bold().fgBrightGreen().a("xctl@${workDir.toFile().name}# ").reset().toString()
                val rightPrompt = null
                var line: String?
                while (true) {
                    try {
                        systemRegistry.cleanUp()
                        line = reader.readLine(prompt, rightPrompt, null as MaskingCallback?, null)
                        systemRegistry.execute(line)
                    } catch (e: UserInterruptException) {
                        // Ignore
                    } catch (_: EndOfFileException) {
                        return@CommandLineRunner
                    } catch (e: Exception) {
                        systemRegistry.trace(e)
                    }
                }
            } catch (t: Throwable) {
                t.printStackTrace();
            } finally {
                AnsiConsole.systemUninstall();
            }
        }
    }
}
