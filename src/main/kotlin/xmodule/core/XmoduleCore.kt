package xmodule.core

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan
class XmoduleCore

val log: Logger = LoggerFactory.getLogger(XmoduleCore::class.java)

